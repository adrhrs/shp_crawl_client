import Vue from 'vue'
import Router from 'vue-router'
import SearchLayout from '@/layout/SearchLayout'
import AnalyserDashboardLayout from '@/layout/AnalyserDashboardLayout'

Vue.use(Router)

export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/dashboard',
      component: AnalyserDashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          component: () => import(/* webpackChunkName: "demo" */ './views/Keyword.vue')
        },
        
      ]
    },
    {
      path: '/sales',
      component: AnalyserDashboardLayout,
      children: [
        {
          path: '/sales',
          name: 'sales',
          component: () => import(/* webpackChunkName: "demo" */ './views/Sales.vue')
        },
        
      ]
    },
    {
      path: '/shop',
      component: AnalyserDashboardLayout,
      children: [
        {
          path: '/shop',
          name: 'shop',
          component: () => import(/* webpackChunkName: "demo" */ './views/Shop.vue')
        },
        
      ]
    },
    {
      path: '/',
      component: SearchLayout,
      children: [
        {
          path: '/',
          component: () => import(/* webpackChunkName: "demo" */ './views/Search.vue')
        }
      ]
      
    },
    { path: "*", redirect: "/" }
  ]
})
